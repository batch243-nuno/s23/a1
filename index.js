console.log("Hello World");

let trainer = {
    Name: "Ash Burn",
    Age: 18,
    Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasur"],
    Friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function(){
        console.log(`Pikachu! I choose you!`);
    }
}
console.log(trainer);
console.log(`Result of Dot notation`);
console.log(trainer.Name);
console.log(`Result of Bracket notation`);
console.log(trainer["Pokemon"]);
console.log(`Result of talk method`);
trainer.talk();

function Pokemon(pokemonName, pokemonLevel){
    this.pokemonName = pokemonName;
    this.pokemonLevel = pokemonLevel;
    this.pokemonHealth = pokemonLevel * 2;
    this.pokemonAttack = pokemonLevel;
    this.skilltackle = function(targetPokemon){
        console.log(`${this.pokemonName} tackles ${targetPokemon.pokemonName}`);
        targetPokemon.pokemonHealth -= this.pokemonAttack;
        console.log(`${targetPokemon.pokemonName}'s health is now reduced to ${targetPokemon.pokemonHealth}.`);
        if (targetPokemon.pokemonHealth <= 0) {
            targetPokemon.faint();
        }
    },
    this.faint = function(){
        console.log(`${this.pokemonName} fainted!`);
    }
}
let pikachu = new Pokemon("Pikachu", 20);
console.log(pikachu);
let charizard = new Pokemon("Charizard", 20);
console.log(charizard);
let squirtle = new Pokemon("Squirtle", 18);
console.log(squirtle);
let bulbasur = new Pokemon("Bulbasur", 15);
console.log(bulbasur);

pikachu.skilltackle(squirtle);
